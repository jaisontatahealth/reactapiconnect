import axios from 'axios';

export const LABS_DATA_DONE = 'LABS_DATA_DONE';
export const LABS_DATA_LOADING = 'LABS_DATA_LOADING';
export const LABS_DATA_FAILED = 'LABS_DATA_FAILED';

export function labsData(requestData, token = undefined){
  return dispatch =>{
    dispatch({ type: LABS_DATA_LOADING, payload: {} });
    axios.post('http://52.66.123.140:9590/Labs/api/v1/searchTestNames',
      requestData
    ).then((res)=>{
      if(res && res.data){
        dispatch({ type: LABS_DATA_DONE, payload: {
          data: res.data.responseData.ListLabTests.tests,
          total: res.data.responseData.homeCollectionCharge
        }});
      }else{
        dispatch({ type: LABS_DATA_FAILED, payload: {}});
      }
    }).catch((err)=>{
      dispatch({ type: LABS_DATA_FAILED, payload: {}});
    });
  }
}
