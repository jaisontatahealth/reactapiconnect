import { LABS_DATA_LOADING, LABS_DATA_DONE, LABS_DATA_FAILED } from '../actions/labs';

const initialState = {
  data: [],
  total: 0,
  loading: false,
  error: false
}

export const labsApis = (state = initialState, action) => {
  switch (action.type) {
    case LABS_DATA_LOADING:
      return {
        ...state,
        loading: true
      }
    case LABS_DATA_DONE:
      return {
        ...state,
        loading: false,
        data: state.data.concat(action.payload.data),
        total: action.payload.total,
        error: false
      }
    case LABS_DATA_FAILED:
      return {
        ...state,
        loading: false,
        data: [],
        total: 0,
        error: true
      }
  }
  return state;
}
