import { combineReducers } from 'redux';
import { labsApis } from './reducers/labsApis';

const allReducers = combineReducers({
  labsApis
});

export default allReducers;
