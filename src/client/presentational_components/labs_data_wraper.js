import React, { Component } from 'react';
import Loading from '../components/loading';
import HomeCollection from '../components/home_collection';

export default class LabsDataWraper extends Component {

  componentDidMount(){
    this.props.labsData({
      consumerUserId: 0,
      search: "bloo",
      pageNumber: 0
    });
  }

  render(){
    const { data, loading } = this.props;
    return(
      <div style={{padding: 10}}>
        {loading && <Loading/>}
        {!loading && data.length == 0 && <span>No data</span>}
        {!loading && data.length > 0 &&
          data.map((item, index)=>{
            return(
              <HomeCollection name={item.name} param={item.param} info={item.info} key={index}/>
            )
          })
        }
      </div>
    )
  }
}
