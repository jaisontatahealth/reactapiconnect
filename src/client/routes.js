import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import Home from './components/home';
import DataWraper from './container_components/labs_data_connection';
import Layout from './components/layout';

const RouteApp = (props) =>(
  <Router>
    <div className="tttttttttttt">
      <Route path="/" component={Layout} />
      <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/read" component={DataWraper} />
      </Switch>
    </div>
  </Router>
);

export default RouteApp;
