import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { labsData } from '../../redux/actions/labs';

import DataWrap from '../presentational_components/labs_data_wraper';

const mapStateToProps = (state) =>{
  return {
    data: state.labsApis.data,
    loading: state.labsApis.loading,
    total: state.labsApis.total,
    error: state.labsApis.error
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ labsData }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(DataWrap);
