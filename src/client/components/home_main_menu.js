import React, { Component } from 'react';

export default class HomeMainMenu extends Component {
  render(){
    return(
      <div style={{flex:1, textAlign: 'center'}}>
        <div>
          <img src={this.props.icon} height={50}/>
        </div>
        <div style={{fontSize: 10}}>
          {this.props.name}
        </div>
      </div>
    )
  }
}
