import React, { Component } from 'react';
import MainLogo from '../assets/images/Tata Health Plus Logo RGB.png';
import styles from './home.css';
import HomeMainMenu from './home_main_menu';
import ConsultIcon from '../assets/images/consult.svg';
import LabsIcon from '../assets/images/lab_tests.svg';
import MedicineIcon from '../assets/images/medicines.svg';
import InstaIcon from '../assets/images/insa_doc.svg';
import Slider from "react-slick";
import HomeSubMenu from './home_sub_menu';

export default class Home extends Component {

  renderSlider = () => {
    var settings = {
      dots: true,
      infinite: true,
      speed: 200,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <Slider {...settings}>
        <HomeSubMenu bgColor={"#1abc9c"} head={"Upload Documents to Health Locker"} subHead={"Safely store all your medical to Health documents at one place"}/>
        <HomeSubMenu bgColor={"#3498db"} head={"Buy Care Packages"} subHead={"Get exciting discounts on lab test, medicines & doctor consultations."}/>
        <HomeSubMenu bgColor={"#f39c12"} head={"Get genuine medicines at best prices"} subHead={"Get medicines delivered at your doorstep."}/>
      </Slider>
    );
  }


  render(){
    return(
      <div>
        <div className={styles.homeMain}>
          <img src={MainLogo} height={25}/>
          <div className={styles.homeMainBox}>
            <HomeMainMenu icon={ConsultIcon} name={"Consult"}/>
            <HomeMainMenu icon={LabsIcon} name={"Lab Tests"}/>
            <HomeMainMenu icon={MedicineIcon} name={"Medicines"}/>
            <HomeMainMenu icon={InstaIcon} name={"Insta Doc"}/>
          </div>
        </div>
        {/* <div style={{marginTop: 150, textAlign: 'center', width: 'inherit'}}>
          {this.renderSlider()}
        </div> */}
      </div>
    )
  }
}
