import React, { Component } from 'react';
import styles from './header.css';
import { Link } from 'react-router-dom';

export default class HeaderSection extends Component {
  render(){
    const { link, icon, activeIcon, name, pathname} = this.props;
    let active= false;
    if(pathname === link){
      active = true;
    }
    return(
      <Link to={link}>
        <div>
          <img src={active?activeIcon:icon} className={styles.headerIcon}/>
        </div>
        <div className={active?styles.headerSectionTextActive:styles.headerSectionText}>
          {name}
        </div>
      </Link>
    )
  }
}
