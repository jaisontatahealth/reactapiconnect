import React, { Component } from 'react';

export default class HomeCollection extends Component {
  render(){
    const { name, param, info } = this.props;
    return(
      <div style={{margin: 10, border: '1px solid', boxShadow: '2px 2px 2px black', padding: 10}}>
        <div>
          {`Name  : ${name}`}
        </div>
        <div>
          {`Param : ${param}`}
        </div>
        <div>
          {`Info  : ${info}`}
        </div>
      </div>
    )
  }
}
