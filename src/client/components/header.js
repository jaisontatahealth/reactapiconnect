import React, { Component } from 'react';
import styles from './header.css';
import { Link } from 'react-router-dom';
import HeaderSection from './header_section';
import HomeIcon from '../assets/icons/home.svg';
import ReadIcon from '../assets/icons/read.svg';
import OrdersIcon from '../assets/icons/orders.svg';
import HelpIcon from '../assets/icons/help.svg';
import HomeIconActive from '../assets/icons/home_active.svg';
import ReadIconActive from '../assets/icons/read_active.svg';
import OrdersIconActive from '../assets/icons/orders_active.svg';
import HelpIconActive from '../assets/icons/help_active.svg';
import Avatar from './avatar.js';
import UserDetails from '../presentational_components/user_details';


export default class Header extends Component {

  constructor(){
    super();
    this.state = {
      showUserDetails: false
    }
  }

  handleAvatarClick = (status) => {
    return()=>{
      this.setState((state, props)=>{
        return {showUserDetails: status};
      });
    }
  }
  
  render(){
    const { pathname } = this.props.location;
    const { showUserDetails } = this.state;
    
    return(
      <div className={styles.header}>
        <div className={styles.headerSection}>
          <HeaderSection name={"HOME"} icon={HomeIcon} activeIcon={HomeIconActive} link={"/"} pathname={pathname}/>
        </div>
        <div className={styles.headerSection}>
          <HeaderSection name={"READ"} icon={ReadIcon} activeIcon={ReadIconActive} link={"/read"} pathname={pathname}/>
        </div>
        <div className={styles.headerSection} onClick={this.handleAvatarClick(!showUserDetails)}>
          <div className={styles.avatarWraper}>
            <Avatar size={70}/>
            <div className={styles.headerSectionText}>
              JAISON
            </div>
          </div>
        </div>
        <div className={styles.headerSection}>
          <HeaderSection name={"MY ORDERS"} icon={OrdersIcon} activeIcon={OrdersIconActive} link={"/orders"} pathname={pathname}/>
        </div>
        <div className={styles.headerSection}>
          <HeaderSection name={"HELP"} icon={HelpIcon} activeIcon={HelpIconActive} link={"/help"} pathname={pathname}/>
        </div>
        {showUserDetails && <UserDetails />}        
      </div>
    )
  }
};
