import React, { Component } from 'react';

export default class HomeSubMenu extends Component {
  render(){
    return(
      <div style={{background: this.props.bgColor, borderRadius: 3, width: '70%', margin: 'auto', padding: 10}}>
        <h1>
          {this.props.head}
        </h1>
        <h3>
          {this.props.subHead}
        </h3>
      </div>
    )
  }
}
