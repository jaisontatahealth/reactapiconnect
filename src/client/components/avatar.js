import React, { Component } from 'react';
import TestImg from '../assets/images/avatar.png';


export default class Avatar extends Component {

  render(){
    const defaultSize = 60;
    const { size } = this.props;
    return (
      <div style={{height: size?size:defaultSize, width: size?size:defaultSize, borderRadius: '50%', margin: 'auto', overflow: 'hidden', zIndex: 1111111111}}>
        <img src={TestImg} height={size?size:defaultSize}/>
      </div>
    )
  }
}
