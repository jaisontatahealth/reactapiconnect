// const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const CompressionPlugin = require("compression-webpack-plugin")

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff)$/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader?module=true' ]
      }
    ]
  },
  // optimization: {
  //   minimize: true,
  //   runtimeChunk: {
  //       name: 'vendor'
  //   },
  //   splitChunks: {
  //       cacheGroups: {
  //           default: false,
  //           commons: {
  //               test: /node_modules/,
  //               name: "vendor",
  //               chunks: "initial",
  //               minSize: 1
  //           }
  //       }
  //   }
  // },
  plugins: [
    new UglifyJsPlugin({
      test: /\.js/,
      cache: true
    }),
    new CompressionPlugin({
      test: /\.js/,
      cache: true,
      algorithm: 'gzip'
    })
  ],
  devServer: {
    port: process.env.PORT || 8050,
    host: "0.0.0.0",
    disableHostCheck: true,
    historyApiFallback: true
  }
};
