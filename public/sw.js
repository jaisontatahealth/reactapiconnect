self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.open('cacheData').then(function(cache) {
            return cache.match(event.request).then(function (response) {
                return response || fetch(event.request).then(function(response) {
                    cache.put(event.request, response.clone());
                    return response;
                });
            });
        })
    );
});


self.addEventListener('install', function(e) {
    e.waitUntil(
        caches.open('cacheData').then(function(cache) {
            return cache.addAll([
                './',
                './index.html',
                './bootstrap.min.css'
            ]);
        })
    );
});
